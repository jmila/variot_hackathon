
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 14 15:05:55 2020

@author: houda.jmila
"""

# Import Librairies
from IPython import get_ipython
def __reset__(): get_ipython().magic('reset -sf')
__reset__()
import sys
import time
import numpy as np
from sklearn.metrics import roc_curve
# inport classifiers
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
import random
import matplotlib.patheffects as PathEffects
from keras.layers import Input, Conv2D, Lambda, Dense, Flatten,MaxPooling2D, concatenate
from keras.models import Model, Sequential
from keras.regularizers import l2
from keras import backend as K
from keras.optimizers import SGD,Adam
from keras.losses import binary_crossentropy
import os
import pickle
import matplotlib.pyplot as plt
from itertools import permutations
from keras.datasets import mnist
from sklearn.manifold import TSNE
from sklearn.svm import SVC
import tensorflow as tf
from sklearn.metrics import (precision_score, recall_score,f1_score, accuracy_score,mean_squared_error,mean_absolute_error, classification_report,balanced_accuracy_score)
from sklearn import metrics
from sklearn.preprocessing import StandardScaler,normalize,Normalizer
from sklearn import model_selection
from sklearn.model_selection import GridSearchCV, StratifiedKFold
from sklearn.utils import shuffle
from collections import Counter
from keras.utils import to_categorical
import pandas as pd 
import sklearn

#fichier pou enregistrer les affichage:

sys.stdout=open("ExploreClassifiers.txt","a")

print('\n####################################\n DATA PREPARATION \n ############################################################### \n')
#################################### DATA PREPARATION ###############################################################
#déclarer les variables de training (x_train1, y_train1) et de test (t1,t2)
#Spécifier la BD

def load_data(path,train_ratio=0):
    data = pd.read_csv(path, header=0)
    ytt = np.asarray(data["Label"])
    Xtt = np.asarray(data.drop(columns=["Label"]))
    #Xtt = np.asarray(data.drop(columns=["sub_cat"]))
    #Xtt = np.asarray(data.drop(columns=["duration"]))

    if(train_ratio==0 or train_ratio==1):
        return Xtt, [],ytt,[]
    else:
        return sklearn.model_selection.train_test_split(Xtt, ytt, test_size=1 - train_ratio, random_state=0)

def scale_features(X_train, X_test, low=-1, upp=1):
    minmax_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(low, upp)).fit(np.vstack((X_train)))  # Transforms features by scaling each feature to a given range(0~1) in order to reinforce dataset and fit training set.
    X_train_scale = minmax_scaler.transform(X_train)
    X_test_scale = minmax_scaler.transform(X_test)
    return X_train_scale, X_test_scale
     
# np.any(Xtt[:, 0] == value)
#, t1, y_train1, t2 = tl.load_data("./data/20Train_FullTest/kddnsl_selected_onehot_encoder_v1.csv",train_ratio=1)
#X,XT,Y,YT = model_selection.train_test_split(x_train1,y_train1, test_size=0.8, random_state=0)
data = pd.read_csv("DataWithBestFeatures.csv", header=0)
ytt = np.asarray(data["Label"])
Xtt = np.asarray(data.drop(columns=["Label"]))
#t,x_PartOfData, tt, y_PartOfData = load_data("DataWithBestFeatures.csv",train_ratio=0)
#x_PartOfData.shape
print('All available data')
print(Counter(ytt))
print(Xtt.shape)
x_PartOfData, t, y_PartOfData, tt = model_selection.train_test_split(Xtt,ytt, test_size=0.9, random_state=0)
print('Taken Part of data ')
print(Counter(y_PartOfData))
print(x_PartOfData.shape)

x_train, x_test, y_train, y_test = model_selection.train_test_split(x_PartOfData,y_PartOfData, test_size=0.2, random_state=0)
#x_train, x_test, y_train, y_test = model_selection.train_test_split(Xtt,ytt, test_size=0.2, random_state=0)
X=x_train
XT=x_test
Y=y_train
YT=y_test
print('total initial Training Data ')
print(Counter(Y))
print(X.shape)
print('total initial Test Data ')
print(Counter(YT))
print(XT.shape)
     
#scale   
#minmax_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(0, 1)).fit(np.vstack((x_PartOfData)))  # Transforms features by scaling each feature to a given range(0~1) in order to reinforce dataset and fit training set.
x_train_scale, x_test_scale = scale_features(X, XT, 0, 1)
# split the dataset, training and validation
#target_names = ['class 0','class 1', 'class 3','class 4','class 5','class 6','class 9','class 10','class 14','class 15','class 16','class 17','class 18','class 20','class 21','class 22']
target_names = ['Alexa_Amazon','CameraD_Link','Camera_Nest','Echo_show5','Google_Home','Hue_Bridge_Philips','Motion_Sensor_D_Link','Smart_Bulb_Tp_Link','Smart_PlugTeckin1','Smart_PlugTeckin2','Smart_Plug_Tp_Link','Smartphone_Huawei']
#target_names = ['class 0','class 1', 'class 3','class 4','class 5','class 6','class 9','class 10','class 14','class 15']


print('\n####################################\n Decision Tree \n ############################################################### \n')

param_grid = {'max_depth': [10, 50, 100, 500, 1000]}
classifier = DecisionTreeClassifier()
#gridsearch
modelclassifier = GridSearchCV(classifier, param_grid, cv=3, scoring='neg_mean_squared_error')
#modelclassifier = GridSearchCV(knn, param_gridknn,cv=5,refit = True)
#train with initial data
start_time = time.time()
modelclassifier.fit(x_train_scale,Y)
#modelclassifier.fit(X,Y)
print("Training time: %s seconds ---" % (time.time() - start_time))

#select the best estimator
modelclassifier=modelclassifier.best_estimator_
#print results
print('Creating Predictor model')
start_time = time.time()
y_pred = modelclassifier.predict(x_test_scale)
print("Prediction time: %s seconds ---" % (time.time() - start_time))

print ('Misclassified samples classifier: %d' % (YT!= y_pred).sum())
print ('Accuracy: %.2f %%' %(accuracy_score(YT, y_pred) * 100))
print ('balanced accuracy score: %.2f %%' %(balanced_accuracy_score(YT, y_pred) * 100))
print(classification_report(YT, y_pred, target_names=target_names))
print('Confusion matrix')
A=metrics.confusion_matrix(YT, y_pred)
print(A)
B=A.diagonal()/A.sum(axis=1)
print(B)
FP = A.sum(axis=0) - np.diag(A)  
FN = A.sum(axis=1) - np.diag(A)
TP = np.diag(A)
TN = A.sum() - (FP + FN + TP)
print(' False positive' )
print (FP)
print(' False negative' )
print (FN)
print(' True positive' )
print (TP)
print('True negative' )
print (TN)

print(' Sensitivity, hit rate, recall, or true positive rate' )
print(TP/(TP+FN))

print(' Specificity or true negative rate')
print(TN/(TN+FP))

print(' Precision or positive predictive value')
print(TP/(TP+FP))

print(' Negative predictive value')
print(TN/(TN+FN))


print('false positive rate ')
print(FP/(FP+TN))

print(' False negative rate')
print(FN/(TP+FN))

print(' False discovery rate')
print(FP/(TP+FP))

print(' Overall accuracy ' )
print(((TP+TN)/(TP+FP+FN+TN)))


print('\n####################################\n Naive Base \n ############################################################### \n')
param_grid = {} 
classifier = GaussianNB()
#gridsearch
modelclassifier = GridSearchCV(classifier, param_grid, cv=3, scoring='neg_mean_squared_error')
#modelclassifier = GridSearchCV(knn, param_gridknn,cv=5,refit = True)
#train with initial data
start_time = time.time()
modelclassifier.fit(x_train_scale,Y)
print("Training time: %s seconds ---" % (time.time() - start_time))
#select the best estimator
modelclassifier=modelclassifier.best_estimator_
#print results
print('Creating Predictor model')
start_time = time.time()
y_pred = modelclassifier.predict(x_test_scale)
print("Prediction time: %s seconds ---" % (time.time() - start_time))

print ('Misclassified samples classifier: %d' % (YT!= y_pred).sum())
print ('Accuracy: %.2f %%' %(accuracy_score(YT, y_pred) * 100))
print ('balanced accuracy score: %.2f %%' %(balanced_accuracy_score(YT, y_pred) * 100))
print(classification_report(YT, y_pred, target_names=target_names))
print('Confusion matrix')
A=metrics.confusion_matrix(YT, y_pred)
print(A)
B=A.diagonal()/A.sum(axis=1)
print(B)
FP = A.sum(axis=0) - np.diag(A)  
FN = A.sum(axis=1) - np.diag(A)
TP = np.diag(A)
TN = A.sum() - (FP + FN + TP)
print(' False positive' )
print (FP)
print(' False negative' )
print (FN)
print(' True positive' )
print (TP)
print('True negative' )
print (TN)

print(' Sensitivity, hit rate, recall, or true positive rate' )
print(TP/(TP+FN))

print(' Specificity or true negative rate')
print(TN/(TN+FP))

print(' Precision or positive predictive value')
print(TP/(TP+FP))

print(' Negative predictive value')
print(TN/(TN+FN))


print('false positive rate ')
print(FP/(FP+TN))

print(' False negative rate')
print(FN/(TP+FN))

print(' False discovery rate')
print(FP/(TP+FP))

print(' Overall accuracy ' )
print(((TP+TN)/(TP+FP+FN+TN)))

'''
print('\n####################################\n Random Forest \n ############################################################### \n')
param_grid = {'max_depth': [10, 100, 500, 1000], 'n_estimators': [ 10, 100,500, 300]} 

classifier = RandomForestClassifier()
#gridsearch
modelclassifier = GridSearchCV(classifier, param_grid, cv=3, scoring='neg_mean_squared_error')
#modelclassifier = GridSearchCV(knn, param_gridknn,cv=5,refit = True)
#train with initial data
start_time = time.time()
modelclassifier.fit(x_train_scale,Y)
print("Training time: %s seconds ---" % (time.time() - start_time))
#select the best estimator
modelclassifier=modelclassifier.best_estimator_
#print results
print('Creating Predictor model')
start_time = time.time()
y_pred = modelclassifier.predict(x_test_scale)
print("Prediction time: %s seconds ---" % (time.time() - start_time))

print ('Misclassified samples classifier: %d' % (YT!= y_pred).sum())
print ('Accuracy: %.2f %%' %(accuracy_score(YT, y_pred) * 100))
print ('balanced accuracy score: %.2f %%' %(balanced_accuracy_score(YT, y_pred) * 100))
print(classification_report(YT, y_pred, target_names=target_names))
print('Confusion matrix')
A=metrics.confusion_matrix(YT, y_pred)
print(A)
B=A.diagonal()/A.sum(axis=1)
print(B)
FP = A.sum(axis=0) - np.diag(A)  
FN = A.sum(axis=1) - np.diag(A)
TP = np.diag(A)
TN = A.sum() - (FP + FN + TP)
print(' False positive' )
print (FP)
print(' False negative' )
print (FN)
print(' True positive' )
print (TP)
print('True negative' )
print (TN)

print(' Sensitivity, hit rate, recall, or true positive rate' )
print(TP/(TP+FN))

print(' Specificity or true negative rate')
print(TN/(TN+FP))

print(' Precision or positive predictive value')
print(TP/(TP+FP))

print(' Negative predictive value')
print(TN/(TN+FN))


print('false positive rate ')
print(FP/(FP+TN))

print(' False negative rate')
print(FN/(TP+FN))

print(' False discovery rate')
print(FP/(TP+FP))

print(' Overall accuracy ' )
print(((TP+TN)/(TP+FP+FN+TN)))


print('\n####################################\n SVM \n ############################################################### \n')
param_grid = {'C': [1, 10, 100, 1000, 10000], 'gamma': [0.01, 0.001, 0.0001], 'kernel': ['rbf']} 
classifier = SVC()
# classifier = OneVsRestClassifier()
#gridsearch
modelclassifier = GridSearchCV(classifier, param_grid, cv=3, scoring='neg_mean_squared_error')
#modelclassifier = GridSearchCV(knn, param_gridknn,cv=5,refit = True)
#train with initial data
start_time = time.time()
modelclassifier.fit(x_train_scale,Y)
print("Training time: %s seconds ---" % (time.time() - start_time))
#select the best estimator
modelclassifier=modelclassifier.best_estimator_
#print results
print('Creating Predictor model')
start_time = time.time()
y_pred = modelclassifier.predict(x_test_scale)
print("Prediction time: %s seconds ---" % (time.time() - start_time))

print ('Misclassified samples classifier: %d' % (YT!= y_pred).sum())
print ('Accuracy: %.2f %%' %(accuracy_score(YT, y_pred) * 100))
print ('balanced accuracy score: %.2f %%' %(balanced_accuracy_score(YT, y_pred) * 100))
print(classification_report(YT, y_pred, target_names=target_names))
print('Confusion matrix')
A=metrics.confusion_matrix(YT, y_pred)
print(A)
B=A.diagonal()/A.sum(axis=1)
print(B)
FP = A.sum(axis=0) - np.diag(A)  
FN = A.sum(axis=1) - np.diag(A)
TP = np.diag(A)
TN = A.sum() - (FP + FN + TP)
print(' False positive' )
print (FP)
print(' False negative' )
print (FN)
print(' True positive' )
print (TP)
print('True negative' )
print (TN)

print(' Sensitivity, hit rate, recall, or true positive rate' )
print(TP/(TP+FN))

print(' Specificity or true negative rate')
print(TN/(TN+FP))

print(' Precision or positive predictive value')
print(TP/(TP+FP))

print(' Negative predictive value')
print(TN/(TN+FN))


print('false positive rate ')
print(FP/(FP+TN))

print(' False negative rate')
print(FN/(TP+FN))

print(' False discovery rate')
print(FP/(TP+FP))

print(' Overall accuracy ' )
print(((TP+TN)/(TP+FP+FN+TN)))

print('\n####################################\n KNN \n ############################################################### \n')

param_grid = {'n_neighbors': [3, 5, 7, 10, 15, 20, 30],'weights': ['uniform', 'distance'],'metric': ['euclidean','manhattan']}
classifier = KNeighborsClassifier(n_neighbors=1)
#gridsearch
modelclassifier = GridSearchCV(classifier, param_grid, cv=3, scoring='neg_mean_squared_error')
#modelclassifier = GridSearchCV(knn, param_gridknn,cv=5,refit = True)
#train with initial data+
start_time = time.time()
modelclassifier.fit(x_train_scale,Y)
print("Training time: %s seconds ---" % (time.time() - start_time))
#select the best estimator
modelclassifier=modelclassifier.best_estimator_
print('Selected features by cvgrid from param_grid = {n_neighbors: [3, 5, 7, 10, 15, 20, 30],weights: [uniform, distance],metric: [euclidean,manhattan]} ')
#print(modelclassifier.best_params_)
#print results
print('Creating Predictor model')
start_time = time.time()
y_pred = modelclassifier.predict(x_test_scale)
print("Prediction time: %s seconds ---" % (time.time() - start_time))

print ('Misclassified samples classifier: %d' % (YT!= y_pred).sum())
print ('Accuracy: %.2f %%' %(accuracy_score(YT, y_pred) * 100))
print ('balanced accuracy score: %.2f %%' %(balanced_accuracy_score(YT, y_pred) * 100))
print(classification_report(YT, y_pred, target_names=target_names))
print('Confusion matrix')
A=metrics.confusion_matrix(YT, y_pred)
print(A)
B=A.diagonal()/A.sum(axis=1)
print(B)
FP = A.sum(axis=0) - np.diag(A)  
FN = A.sum(axis=1) - np.diag(A)
TP = np.diag(A)
TN = A.sum() - (FP + FN + TP)
print(' False positive' )
print (FP)
print(' False negative' )
print (FN)
print(' True positive' )
print (TP)
print('True negative' )
print (TN)

print(' Sensitivity, hit rate, recall, or true positive rate' )
print(TP/(TP+FN))

print(' Specificity or true negative rate')
print(TN/(TN+FP))

print(' Precision or positive predictive value')
print(TP/(TP+FP))

print(' Negative predictive value')
print(TN/(TN+FN))

print('false positive rate ')
print(FP/(FP+TN))

print(' False negative rate')
print(FN/(TP+FN))

print(' False discovery rate')
print(FP/(TP+FP))

print(' Overall accuracy ' )
print(((TP+TN)/(TP+FP+FN+TN)))

'''


